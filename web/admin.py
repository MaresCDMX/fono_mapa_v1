from django.contrib import admin
from .models import About, Instructions


admin.site.register(About)
admin.site.register(Instructions)
