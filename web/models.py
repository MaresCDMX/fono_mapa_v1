from __future__ import unicode_literals

from django.db import models
from ckeditor.fields import RichTextField


class About(models.Model):
    title = models.CharField(blank=True, null=True, max_length=100)
    content = RichTextField()

    def __str__(self):
        return 'Acerca de'


class Instructions(models.Model):
    title = models.CharField(blank=True, null=True, max_length=100)
    content = RichTextField()

    def __str__(self):
        return 'Instrucciones'
