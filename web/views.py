from django.shortcuts import render
from django.shortcuts import HttpResponse
import json
from audio.models import Audio
from audio.views import Audio as AudioObject
from userprofile.views import UserProfile


def home(request):
    return render(request, 'base.html', {
        'message': '',
        'audio_counter': Audio.objects.count(),
    })


def upload_audio(request):
    audio_object = AudioObject()
    return audio_object.create_audio(request)


def sign_in(request):
    return UserProfile.sign_in(request)


def sign_up(request):
    return UserProfile.sign_up(request)
