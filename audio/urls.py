#from django.conf.urls import url
from django.urls import include, path, re_path
from django.views.decorators.csrf import csrf_exempt
from .viewsets import *
from .models import Audio, Tag
from dal import autocomplete
from rest_framework import views

urlpatterns = [
    path(r'^audio/$', views.index, name='audio'),
    url(r'^get_audio/$', csrf_exempt(get_audio)),
    include(
        r'^test-autocomplete/',
        autocomplete.Select2QuerySetView.as_view(
            model=Tag,
            create_field='tag_name',
        ),
        name='tags_autocomplete'
    ),
    url(r'^get_tag/', TagAPI.as_view()),
    url(r'^get_all_tags/', get_all_tags),
    url(r'^get_fonografista/', get_fonografista),
    url(r'^get_audio_by_fono/', get_audio_by_fono),
    url(r'^get_all_fonos/', get_all_fonos)
]
