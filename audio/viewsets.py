from rest_framework import viewsets
from rest_framework.decorators import api_view
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import status
from rest_framework.response import Response
from django.shortcuts import HttpResponse
from .serializers import AudioSerializer, TagSerializer, UserSerializer
from rest_framework.views import APIView
from django.contrib.auth.models import User
from django.db.models import Count
from django.views.decorators.csrf import csrf_exempt
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Audio, Revision, Tag, Location
from django.db.models import Q
import json


@api_view(['GET'])
def audio(request):
    if request.method == 'GET':
        revisiones = Revision.objects.values_list(
            'audio', flat=True).filter(aproved=True).filter(published=True)
        audios = Audio.objects.filter(id__in=revisiones)
        serializer = AudioSerializer(audios, many=True)
        return Response(serializer.data)


@api_view(['POST'])
def get_audio(request):
    tipo = int(request.POST.get('tipo'))
    busqueda = request.POST.get('busqueda')
    data = None
    revisiones_list = list(
        Revision.objects.values_list(
            'audio', flat=True
        ).filter(
            aproved=True
        ).filter(
            published=True
        )
    )
    if tipo == 1:
        if busqueda:
            try:
                tag = Tag.objects.filter(tag_name=busqueda)
                audios = Audio.objects.filter(tag__in=tag).filter(
                    id__in=revisiones_list)
            except Exception as e:
                data = {
                    'status': status.HTTP_404_NOT_FOUND,
                    'detail': str(e)
                }
                return HttpResponse(
                    json.dumps(data), content_type="application/json"
                )
        else:
            tag = Tag.objects.all()
            audios = Audio.objects.filter(tag__in=tag).filter(
                id__in=revisiones_list)
        serializer = AudioSerializer(audios, many=True)
        return Response(serializer.data)
    if tipo == 2:
        if busqueda:
            try:
                estado = Location.objects.filter(state=busqueda)
                audios = Audio.objects.filter(location__in=estado).filter(
                    id__in=revisiones_list)
            except Exception as e:
                data = {
                    'status': status.HTTP_404_NOT_FOUND,
                    'detail': str(e)
                }
                return HttpResponse(
                    json.dumps(data), content_type="application/json"
                )
        serializer = AudioSerializer(audios, many=True)
        return Response(serializer.data)
    if tipo == 3:
        if busqueda:
            try:
                fonografista = User.objects.get(username=busqueda)
                audios = Audio.objects.filter(owner=fonografista).filter(
                    id__in=revisiones_list)
            except Exception as e:
                data = {
                    'status': status.HTTP_404_NOT_FOUND,
                    'detail': str(e)
                }
                return HttpResponse(
                    json.dumps(data), content_type="application/json"
                )
        else:
            audios = Audio.objects.all().filter(
                id__in=revisiones_list)
        serializer = AudioSerializer(audios, many=True)
        return Response(serializer.data)
    if tipo == 4:
        if busqueda:
            try:
                tag = Tag.objects.filter(tag_name=busqueda)
                estado = Location.objects.filter(state=busqueda)
                fonografista = User.objects.filter(username=busqueda)
                audios = Audio.objects.filter(Q(tag__in=tag)
                    | Q(location__in=estado)
                    | Q(owner__in=fonografista)
                    | Q(audio_title__icontains=busqueda)
                    | Q(audio_description__icontains=busqueda)).filter(id__in=revisiones_list).distinct()
            except Exception as e:
                data = {
                    'status': status.HTTP_404_NOT_FOUND,
                    'detail': str(e)
                }
                return HttpResponse(
                    json.dumps(data), content_type="application/json"
                )
        else:
            audios = Audio.objects.all().filter(
                id__in=revisiones_list)
        serializer = AudioSerializer(audios, many=True)
        return Response(serializer.data)
    if tipo == 5:
        if busqueda:
            try:
                audios = Audio.objects.filter(audio_title__icontains=busqueda).filter(
                    id__in=revisiones_list)
            except Exception as e:
                data = {
                    'status': status.HTTP_404_NOT_FOUND,
                    'detail': str(e)
                }
                return HttpResponse(
                    json.dumps(data), content_type="application/json"
                )
        else:
            audios = Audio.objects.all().filter(
                id__in=revisiones_list)
        serializer = AudioSerializer(audios, many=True)
        return Response(serializer.data)
    else:
        audios = Audio.objects.filter(id__in=revisiones_list)
        serializer = AudioSerializer(audios, many=True)
        return Response(serializer.data)


class TagAPI(APIView):
    def count_coincidencias(self, lista):
        veces = []
        objec = {}
        revisiones = Revision.objects.values_list(
            'audio', flat=True).filter(aproved=True).filter(published=True)
        try:
            for o in lista:
                objec = {}
                objec['estado'] = o
                objec['cantidad'] = len(Location.objects.filter(state=o))
                objec['tags'] = list(
                    Tag.objects.values_list('tag_name').filter(
                        id__in=list(
                            Audio.objects.values_list(
                                'tag', flat=True
                            ).filter(
                                location__in=Location.objects.filter(state=o)
                            ).filter(
                                id__in=revisiones
                            ).annotate(
                                repetidos=Count('tag')
                            ).order_by('-repetidos')
                        )
                    )[:2]
                )
                veces.append(objec)
        except:
            veces = veces
        return veces

    def get(self, request, format=None):
        obj = {}
        estados_cls = list(set(list(
            Location.objects.values_list(
                'state', flat=True
            ).exclude(state__isnull=True).annotate(
                repetidos=Count('state')
            ).order_by('repetidos')))
        )[:5]
        obj['estados'] = self.count_coincidencias(estados_cls)
        return HttpResponse(
            json.dumps(obj), content_type='application/json'
        )

    def post(self, request, format=None):
        busqueda = request.POST.get('estado')
        revisiones = Revision.objects.values_list(
            'audio', flat=True).filter(aproved=True).filter(published=True)
        try:
            audios = Audio.objects.filter(id__in=revisiones).filter(
                location__in=Location.objects.filter(state=busqueda)
            )
            serializer = AudioSerializer(audios, many=True)
            return Response(serializer.data)
        except Exception as e:
            data = {
                'status': status.HTTP_404_NOT_FOUND,
                'detail': str(e)
            }
            return HttpResponse(
                json.dumps(data), content_type="application/json"
            )


@csrf_exempt
@api_view(['POST'])
def get_all_tags(request):
    data = {}
    tags_list = []
    page = 1
    paginator = Paginator(Tag.objects.all(), 20)
    try:
        page = int(request.POST.get('page'))
    except:
        page = 1
    try:
        tags = paginator.page(page)
    except PageNotAnInteger:
        tags = paginator.page(1)
    except EmptyPage:
        tags = paginator.page(paginator.num_pages)
    tags_list.append(TagSerializer(tags.object_list, many=True).data)
    data['num_pages'] = paginator.num_pages
    data['tags'] = tags_list
    return HttpResponse(json.dumps(data), content_type='application/json')


@api_view(['GET'])
def get_fonografista(request):
    revisiones = Revision.objects.values_list(
        'audio', flat=True).filter(aproved=True).filter(published=True)
    fonografista_list = list(set(list(
        Audio.objects.values_list('owner', flat=True).filter(
            id__in=revisiones
        ).annotate(
            repetidos=Count('owner')).order_by('repetidos'))
        )
    )
    fonografista = User.objects.filter(
        id__in=fonografista_list).order_by('first_name')[:5]
    serializer = UserSerializer(fonografista, many=True)
    return Response(serializer.data)


@api_view(['POST'])
def get_audio_by_fono(request):
    fonografista = User.objects.filter(
        username=request.POST.get('username')
    )
    audios = Audio.objects.filter(owner=fonografista)
    serializers = AudioSerializer(audios, many=True)
    return Response(serializer.data)


@api_view(['POST'])
def get_all_fonos(request):
    data = {}
    fono_list = []
    page = 1
    revisiones = Revision.objects.values_list(
        'audio', flat=True).filter(aproved=True).filter(published=True)
    fonografista_list = list(set(list(
        Audio.objects.values_list('owner', flat=True).filter(
            id__in=revisiones
        ).annotate(
            repetidos=Count('owner')).order_by('repetidos'))
        )
    )
    paginator = Paginator(
        User.objects.filter(id__in=fonografista_list).order_by('first_name'),
        20
    )
    try:
        page = request.POST.get('page')
    except:
        page = 1
    try:
        fonos = paginator.page(page)
    except PageNotAnInteger:
        fonos = paginator.page(1)
    except EmptyPage:
        fonos = paginator.page(paginator.num_pages)
    fono_list.append(UserSerializer(fonos.object_list, many=True).data)
    data['num_pages'] = paginator.num_pages
    data['tags'] = fono_list
    return HttpResponse(json.dumps(data), content_type='application/json')
