# coding=utf-8
from django.core.mail import EmailMessage
from django.conf import settings


def send_notification():
    asunto = 'Nuevo audio recibido'
    mensaje = 'Un usuario público a cargado un nuevo audio desde el mapa sonoro, para revisarlo, autorizarlo y publicarlo entra al administrador.'
    mail = EmailMessage(
        asunto,
        mensaje,
        from_email=settings.EMAIL_HOST_USER,
        to=['mapasonoro@cultura.gob.mx']
    )
    mail.send()