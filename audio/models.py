# -*- coding: utf-8 -*-
from django import forms
from django.db import models
from django.db.models import Model
from django.contrib.auth.models import User
from .storage import UploadToPath


class BinaryData(Model):
    geeks_field = models.BinaryField()

class Tag(models.Model):
    tag_name = models.CharField('Tag', max_length=100)

    class Meta:
        verbose_name = 'Tag'
        verbose_name_plural = 'Tags'

    def __str__(self):
        return str(self.tag_name)


class Location(models.Model):
    location_name = models.CharField('Nombre de la locación', max_length=200)
    country = models.CharField('Pais', null=True, blank=True, max_length=150)
    state = models.CharField('Estado', null=True, blank=True, max_length=150)
    town = models.CharField('Pueblo', null=True, blank=True, max_length=200)
    formated_address = models.CharField('Dirección completa',
                                        null=True, blank=True,
                                        max_length=300)
    place_id = models.CharField('PlaceID de google',
                                null=True, blank=True,
                                max_length=50)
    latitude = models.FloatField('Latitud', null=True, blank=True)
    longitude = models.FloatField('Longitud', null=True, blank=True)
    description = models.TextField('Descripción del lugar',
                                   null=True, blank=True,)
    location_image = models.FileField(upload_to=UploadToPath('files/%Y/%m/%d'), max_length=255)
    

    class Meta:
        verbose_name = 'Ubicación'
        verbose_name_plural = 'Ubicaciones'

    def __str__(self):
        return str(self.location_name)

class Comment(models.Model):
    comment_user = models.ForeignKey(User,on_delete=models.CASCADE)
    comment = models.TextField('Comentarios')
    created_date = models.DateTimeField('Fecha de creación',
                                        auto_now=False, auto_now_add=True)
    updated_date = models.DateTimeField('Fecha de modificación',
                                        auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name = 'Comentario'
        verbose_name_plural = 'Comentarios'

    def __str__(self):
        return '{}'.format(self.comment_user)

class Author(models.Model):
    name = models.CharField('Nombre de autor', max_length=255)

    class Meta:
        verbose_name = 'Autor'
        verbose_name_plural = 'Autores'

    def __str__(self):
        return str(self.name)


class Record(models.Model):
    record_date = models.DateField('Fecha de grabación')
    record_time = models.TimeField('Hora de grabación', null=True, blank=True)
    record_description = models.TextField('Descripción de la grabación', null=True, blank=True)

    class Meta:
        verbose_name = 'Información de grabación'
        verbose_name_plural = 'Información de grabaciones'

    def __str__(self):
        return str(self.record_description)#'{}'.format(self.pk)


class Audio(models.Model):
    audio_title = models.CharField('Titulo del audio', max_length=300)
    audio_duration = models.TimeField('Duración', null=True, blank=True)
    audio_file = models.FileField(upload_to=UploadToPath('files/%Y/%m/%d'), max_length=255)
    
    owner = models.ForeignKey(User, verbose_name='Usuario', on_delete=models.CASCADE)
    author = models.ManyToManyField(Author, verbose_name='Autor',
                                    null=True, blank=True, default=None)
    upload_date = models.DateField('Fecha de subida',
                                   auto_now_add=True)
    tag = models.ManyToManyField(Tag, verbose_name='Tags',
                                 null=True, blank=True)
    location = models.ForeignKey(Location, verbose_name='Ubicación', on_delete=models.CASCADE)
    record_information = models.ForeignKey(Record, verbose_name='Información de grabación', on_delete=models.CASCADE)
    audio_description = models.TextField(
        'Descripción del audio', null=True, blank=True, max_length='1000')

    def get_player(self):
        return '<audio controls preload="none"><source src="{}" type="audio/mpeg"/></audio>'.format(self.audio_file.url)

    def is_approved(self):
        return Revision.objects.get(audio=self).aproved

    def is_published(self):
        return Revision.objects.get(audio=self).published

    def save(self, *args, **kwargs):
        super(Audio, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Audio'
        verbose_name_plural = 'Audios'

    def __str__(self):
        return str(self.audio_title)


class Revision(models.Model):
    revisor = models.ForeignKey(User, verbose_name='Revisor', on_delete=models.CASCADE)
    aproved = models.BooleanField('Aprobado', default=False)
    published = models.BooleanField('Publicado', default=False)
    comment = models.ManyToManyField(
        Comment, verbose_name='Comentarios',
        null=True, blank=True)
    audio = models.ForeignKey(
        Audio, default=True, verbose_name='audio', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Revisión'
        verbose_name_plural = 'Revisiones'

    def __str__(self):
        return '{}'.format(self.revisor.username)

