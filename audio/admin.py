# -*- coding: utf8 -*-
from django.contrib import admin
from .models import Tag
from .models import Location, Comment, Author
from .models import Record, Revision, Audio
from .forms import AudioAdminForm
from .forms import RevisionAdminForm


@admin.register(Audio)
class AudioAdmin(admin.ModelAdmin):
    form = AudioAdminForm
    ordering = ('-id',)
    list_per_page = 10
    list_display = (
        'id', 'audio_title', 'audio_file', 'reproductor', 'get_adress',
        'get_aproved', 'get_published', 'owner'
        )

    class Media:
        css = {
            "all": (
                "https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css",
            )
        }
        js = (
            "https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js",
            "https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js",
            "js/select2.js"
        )

    def get_adress(self, obj):
        return obj.location.formated_address
    get_adress.short_description = 'Dirección'

    def reproductor(self, obj):
        return obj.get_player()
    reproductor.allow_tags = True
    reproductor.short_description = 'Audio'

    def get_aproved(self, obj):
        return obj.is_approved()
    get_aproved.boolean = True
    get_aproved.short_description = 'Aprobado'

    def get_published(self, obj):
        return obj.is_published()
    get_published.boolean = True
    get_published.short_description = 'Publicado'


@admin.register(Revision)
class RevisionAdmin(admin.ModelAdmin):
    form = RevisionAdminForm
    ordering = ('published','aproved',)
    list_per_page = 10
    list_display = ('revisor', 'aproved', 'published', 'audio')

@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    ordering = ('tag_name',)
    list_per_page = 10

@admin.register(Location)
class LocationAdmin(admin.ModelAdmin):
    ordering = ('location_name',)
    list_per_page = 10

@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    ordering = ('-created_date',)
    list_per_page = 10

@admin.register(Author)
class AuthorAdmin(admin.ModelAdmin):
    ordering = ('name',)
    list_per_page = 10

@admin.register(Record)
class RecordAdmin(admin.ModelAdmin):
    ordering = ('record_description',)
    list_per_page = 10
