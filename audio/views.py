from django.shortcuts import render

# Create your views here.
from django.shortcuts import redirect
from django.shortcuts import HttpResponse
import json

from django.views import generic
from django.urls import reverse
from django.contrib.auth.models import User
from .models import Audio as AudioModel
from .models import Location
from .models import Record, Tag, Author
from .mails import send_notification
from django.core.exceptions import ValidationError


class Audio(object):

    def create_audio(self, request):
        try:
            message = None
            user = User.objects.get(username=request.user)
            audi_file_name = request.FILES['audio_file'].name

            if audi_file_name.endswith('.mp3'):
                pass
            elif audi_file_name.endswith('.ogg'):
                pass
            elif audi_file_name.endswith('.m4a'):
                pass
            else:
                raise ValidationError('Extencion de archivo no soportado, solo mp3, ogg o m4a')
            if 'title' in request.POST and 'audio_file' in request.FILES:
                data = request.POST
                files = request.FILES

                # Inicio del bloque que crea o recupera una ubicación
                location_data = json.loads(data['location_data'])
                location, location_create = Location.objects.get_or_create(location_name=data['record_place'])

                if location_create:
                    if data['geometry'] and data['geometry'] is not '':
                        geometry = json.loads(data['geometry'])
                        location.latitude = geometry['location']['lat']
                        location.longitude = geometry['location']['lng']
                    else:
                        location.latitude = data['lat']
                        location.longitud = data['lng']

                    try:
                        location.location_image = files['photo']
                    except:
                        pass
                    location.formated_address = data['record_place']
                    for item in location_data:
                        if item['types'][0] == 'country':
                            location.country = item['long_name']
                        if item['types'][0] == 'administrative_area_level_1':
                            location.state = item['long_name']
                        if item['types'][0] == 'sublocality':
                            location.town = item['long_name']
                    location.save()
                # Fin del bloque de ubicación
                # Inicia el bloque que crea un nuevo registro con la
                # información 3de la grabación
                tags_list = data['tags'].split('#')
                tags_list_cls = []
                for tag in tags_list:
                    if tag is not None and tag != '':
                        tags_obj, created = Tag.objects.get_or_create(
                            tag_name=tag
                        )
                        tags_list_cls.append(tags_obj)
                record_information = Record.objects.create(
                    record_date=data['record_date'] if data['record_date'] else None,
                    record_time=data['record_time'] if data['record_time'] else None,
                    record_description=data['record_description'] if data['record_description'] else None
                )
                # Termina el bloque de información

                # Se crea el nuevo audio
                new_audio = AudioModel.objects.create(
                    owner=user,
                    location=location,
                    record_information=record_information,
                    audio_title=data['title'],
                    audio_file=files['audio_file'],
                    audio_description=data['content_description'] if data['content_description'] else None
                )
                author, created = Author.objects.get_or_create(name=data['autor'])
                for tag in tags_list_cls:
                    new_audio.tag.add(tag)
                new_audio.author.add(author)
                new_audio.save()
                send_notification() # Antes se hacía en .models Audio save
                message = 'Tu audio será revisado'

            ctx = {
                'message': message,
                'audio_counter': AudioModel.objects.count(),
            }
            return redirect('/')
        except Exception as e:
            return HttpResponse(json.dumps({'message': str(e)}), status=400)
