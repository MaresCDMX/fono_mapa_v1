# -*- coding: utf-8 -*-
from rest_framework import serializers

from .models import Audio
from .models import Location
from .models import Tag, Record, Author
from django.contrib.auth.models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username',)


class RecordSerializer(serializers.ModelSerializer):
    class Meta:
        model = Record
        fields = '__all__'


class LocationSerializer(serializers.ModelSerializer):
    img = serializers.SerializerMethodField('get_image_url')

    def get_image_url(self, obj):
        try:
            if obj.location_image.url is None:
                return ''
            else:
                return obj.location_image.url
        except:
            return ''

    class Meta:
        model = Location
        fields = [
            'location_name', 'latitude', 'longitude', 'formated_address',
            'img'
        ]


class AudioSerializer(serializers.ModelSerializer):
    location = LocationSerializer(many=False, read_only=True)
    audio_url = serializers.URLField(read_only=True, source='audio_file.url')
    record_information = RecordSerializer(many=False, read_only=True)
    owner = UserSerializer(many=False, read_only=True)
    author = serializers.SerializerMethodField()

    class Meta:
        model = Audio
        fields = [
            'id','audio_title', 'audio_description', 'audio_url',
            'location', 'tag', 'upload_date',
            'record_information', 'owner', 'author'
        ]
        depth = 1

    def get_author(self, obj):
        return list(
            Author.objects.values_list(
                'name', flat=True
            ).filter(
                id__in=obj.author.all()
            )
        )


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = '__all__'
