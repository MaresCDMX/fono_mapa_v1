# coding=utf-8
import os

from dal import autocomplete

from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from .models import Audio, Revision

class AudioAdminForm(forms.ModelForm):
    class Meta:
        model = Audio
        exclude = ()
        widgets = {
            'tag': forms.SelectMultiple(attrs={'class': 'select2'}),
            'author': forms.SelectMultiple(attrs={'class': 'select2'})
        }

class RevisionAdminForm(forms.ModelForm):
    class Meta:
        model = Revision
        fields = ('revisor', 'aproved', 'published', 'comment', 'audio')

    def clean(self):
        data = self.cleaned_data
        audio_data = data['audio']
        revision = Revision.objects.filter(audio=audio_data)
        if revision.count() > 1:
            raise ValidationError('Ya existe una revision para el audio')
        return data
