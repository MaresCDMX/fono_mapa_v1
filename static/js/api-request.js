var request = new XMLHttpRequest();

var urlApi = 'https://maps.googleapis.com/maps/api/geocode/json';
var key = '&key=AIzaSyAyjPImP1kk-Hh31cxk0r7fvFcksun_rCs';//AIzaSyBNKPRl_Bg8dpyC5xz7s679nrDoRMkVTmQ';
var latlng = lat + ',' + lng;
var urlRequest;


// peticion a google places
function getGeoData(functionName) {
  var data;
  urlRequest = urlApi + '?latlng=' + latlng + key;
  request.open('GET', urlRequest, true);

  request.onload = function () {
      if (request.status >= 200 && request.status < 400) {
        data = JSON.parse(request.responseText);
        geo_data = [data.results[0]];
        executeFunction[functionName]();
      } else {
        // We reached our target server, but it returned an error

      }
  };

  request.onerror = function () {
      console.log('error');
  }

  request.send();

}

// funcion para recuperar el token de django

function getCookie(name) {
    var cookieValue = null
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';')
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim()
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1))
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');
var request;

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method))
}

// peticion POST

function postRequest(url, data) {
  var request = new XMLHttpRequest();
  request.open('POST', url, true);
  request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
  request.setRequestHeader("X-CSRFToken", csrftoken);

  request.onload = function() {
    if (request.status >= 200 && request.status < 400) {
      // Success!
      var resp = request.responseText;
      console.log(resp);
    } else {
      // We reached our target server, but it returned an error

    }
  };

  request.onerror = function() {
    // There was a connection error of some sort
  };

  request.send(data);
}

function getRequest(url, data) {
  var request = new XMLHttpRequest();
  request.open('GET', url, true);
  request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
  request.setRequestHeader("X-CSRFToken", csrftoken);
  request.send(data);
  return request;
}
