/* Inicializando el mapa */
var popup = document.querySelector('.popup-click');
var makePin = document.querySelector('.js-make-pin');
var uploadForm = document.querySelector('.upload-form');
var cierra = document.querySelector('.cierra');
var buscar = document.querySelector('#buscar_tipo');
var button_tags = document.querySelector('.tags');
var todo_tag =document.querySelector('#click_todo_tag');
var modal_tag = document.querySelector('#modal-tags');
var fono_tag = document.querySelector('#ul_fono');
var change_pass = document.querySelector('#change_pass');
var show_all = document.querySelector('#show_all');
var map;
var oms;
var lat;
var lng;
var markers = [];
var geo_data;
var display_marker = [];
var staticMarkers = [];
var data_response;
var to_search;
var valor = '';
var markerCluster = null;

var executeFunction = {
    create_card : function () {
        createCard();
    }
};

/* Variables para detectar long touch */
var timer, lockTimer = false;
var press;
var touchduration = 800; // Duración que debe presionar el usuario antes de considerarlo como long touch
var islongtouch = false; // Para que no se esconda el pop up en el evento de click al momento de soltar el touch
var onlongtouch = function() { 
    islongtouch = true;
    showCreatePin(press);
};

/* Función para detectar si es un móvil */
window.mobilecheck = function() {
    var check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
    return check;
};

/* Eventos del mapa */
function initAutocomplete() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat:23.9698885, lng: -99.1179815},
        zoom: 5
    });

    map.setOptions(
        {
            styles: styles
        }
    );

    map.addListener('rightclick', function(e) {
        showCreatePin(e);
    });

    map.addListener('dblclick', function() {
        popup.setAttribute('style', 'visibility: collapse;');
    });

    map.addListener('click', function() {
        if(!islongtouch) popup.setAttribute('style', 'visibility: collapse;');
        islongtouch = false;
    });

    // Para detectar long press
    map.addListener("mousedown", function(e) {
        // Touch start
        press = e;
        if(lockTimer) return;
        timer = setTimeout(onlongtouch, touchduration); 
        lockTimer = true;
    });
    map.addListener("mouseup", function() {
        // Stops short touches from firing the event
        if (timer) {
            clearTimeout(timer); // clearTimeout, not cleartimeout..
            lockTimer = false;
        }
    });
    map.addListener("drag" ,function() {
        // Stops map dragging from firing the event
        if (timer) {
            clearTimeout(timer); // clearTimeout, not cleartimeout..
            lockTimer = false;
        }
    });

    // Crea instancia de OverlappingMarkerSpiderfier asociada al mapa para funcionalidad cuando hay varios audios en el mismo punto
    oms = new OverlappingMarkerSpiderfier(map, {
        markersWontMove: true,
        markersWontHide: true,
        basicFormatEvents: true
    });

    // Obtiene los audios
    addStaticMarker();

    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);

    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    map.addListener('bounds_changed', function() {
        searchBox.setBounds(map.getBounds());
    });

    searchBox.addListener('places_changed', function() {
        var places = searchBox.getPlaces();
        var bounds = new google.maps.LatLngBounds();

        places.forEach(function(place) {
            if (place.geometry.viewport) {
                bounds.union(place.geometry.viewport);
            }
            else {
                bounds.extend(place.geometry.location);
            }
        });

        map.fitBounds(bounds);
    });
    login();

    // Tooltip (Creative Commons)
    $('[data-toggle=tooltip]').tooltip();

    // Cuando se muestre el modal de búsqueda enfoca el input
    $('#modal-buscador').on("shown.bs.modal", function () { 
        $('#busqueda').focus();
    });

    // Esconde el menú en móvil cuando dan click en un link que no es dropdown
    $(".navbar-nav li").on('click',function(event) {
        if(!$(this).hasClass('dropdown')) $(".navbar-collapse").collapse('hide');
    });
}

function addStaticMarkerAjax(data){
    if (markerCluster) {
        markerCluster.clearMarkers();
        markers = [];
        markerCluster = new MarkerClusterer(map, markers);
    }

    for(var item in data) {
        pin = data[item];
        if(pin.location.latitude && pin.location.longitude) {
            latLng = {
                lat: pin.location.latitude, lng: pin.location.longitude
            };
            title = pin.audio_title;
            var marker = new google.maps.Marker({
                position: latLng,
                // map: map,
                title: title,
                customData: pin
            });
            markers.push(marker);
            attachInfoMarker(
                marker, pin.id, pin.audio_url, title, pin.audio_description, pin.location_name,
                pin.location.formated_address != null ? pin.location.formated_address : (pin.location.location_name != null ? pin.location.location_name : 'Sin lugar de grabación'),
                pin.record_information.record_date != null ? pin.record_information.record_date : 'Sin fecha de grabación', pin.tag,
                pin.record_information.record_description,
                pin.record_information.record_time,
                pin.author[0]
            );
            google.maps.event.addListener(marker, 'spider_click', function(){
                var div_marker = $('#iw-audio-'+ this.customData.id);
                if(pin.location.img != '' && !div_marker.children().length) {
                    div_marker.append('<img id="iw-img-'+ this.customData.id +'" src="'+this.customData.location.img.replace('http://','https://')+'" />');
                }
            });
            staticMarkers.push(marker);
            oms.addMarker(marker);
        }
    }

    var mcOptions = {gridSize: 50, maxZoom: 15, imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'}

    markerCluster = new MarkerClusterer(map, markers, mcOptions);

}

function showCreatePin(e) {
    var cursorX = e.pixel.x;
    var cursorY = e.pixel.y;
    popup.setAttribute('style', 'visibility: visible; left: ' + cursorX + 'px; top: ' + cursorY + 'px;');
    lat = e.latLng.lat();
    lng = e.latLng.lng();
}

// Funciones para mostrar mensajes de éxito y errores
function mostrarExito(mensaje) {
    $('#modal-exito').find('.message').html(mensaje);
    $('#modal-exito').modal('show');
}
function mostrarError(mensaje) {
    $('#modal-error').find('.message').html(mensaje);
    $('#modal-error').modal('show');
}
function mostrarErrorBusqueda() {
    mostrarError('No se encontraron resultados para tu búsqueda');
}

// agregando marcadores al mapa
function addMarker(lat, lng) {
    var latLng = {
        lat: lat, lng: lng
    };
    var marker = new google.maps.Marker({
        position: latLng,
        // map: map,
        title: title
    });

    display_marker.push(marker);
    oms.addMarker(marker);
}

function removeMarker(){
    if (display_marker !== null){
        for (var i = 0; i < display_marker.length; i++) {
            display_marker[i].setMap(null);
        }
    }
    display_marker = [];
}

function removeStaticMarker(){
    if(staticMarkers !== null){
        for (var i = 0; i < staticMarkers.length; i++) {
            staticMarkers[i].setMap(null);
        }
    }
    staticMarkers = [];
}

function toDate(fecha){
    var options = {weekday: "long", year: "numeric", month: "long", day: "numeric"};
    var nueva_fecha = new Date(fecha + 'T13:00:00.000Z'); // Se le agrega la hora para que no le reste un día por la zona horaria
    return nueva_fecha.toLocaleString("es-ES", options);
}

function attachInfoMarker(marker, id, url, title, detail, location_name, formated_address, upload_date, tags, record_description, record_time, username) {
    var tagsString = "";
    for (var index in tags){
        tagsString = tagsString + "<span class='tag_info_window' id='tag-"+ tags[index].tag_name +"' onclick='estados_tag(event)'>#" + tags[index].tag_name + " </span>";
    }
    var contenido = '<div class="iw-container">' +
                        '<div class="iw-title">'+title+'</div>' +
                        '<div id="iw-audio-'+ id +'" style="width:100%;padding: 0 15px;"></div>'+
                        '<div class="iw-content">' +
                            '<h5>Escuchar</h5>'+
                            '<audio preload="none" controls><source src="' + url.replace('http://','https://') + '" type="audio/'+(url.includes('.m4a')?'mp4':(url.includes('.ogg')||url.includes('.oga')?'ogg':'mpeg'))+'"/></audio>'+
                            // AddType audio/mpeg mp3
                            // AddType audio/mp4 m4a
                            // AddType audio/ogg ogg oga
                            '<p><b>' + formated_address + '</b></p>'+
                            '<h5>Fecha:</h5>'+
                            '<p>'+ toDate(upload_date) +'</p>'+
                            '<h5><b>Información: </b></h5>'+
                            '<p> ' + detail + '</p>'+
                            (record_time?'<h5>Hora de grabación:</h5>'+'<p>'+ record_time +'</p>':'')+
                            (tagsString?'<h5>TAGS:</h5>'+'<p>'+ tagsString +'</p>':'')+
                            (record_description?'<h5>Equipo utilizado: </h5>'+'<p>' + record_description + '</p>':'')+
                            (username?'<h5>Nombre de quien realizó la grabación:</h5>'+'<p>'+username+'</p>' : '')+
                        '</div>'+
                    '</div>';
    var infowindow = new google.maps.InfoWindow({
        content: contenido,
        width: 300,
        maxWidth: 300,
        disableAutoPan: false,
        // pixelOffset: new google.maps.Size(20,0)
    });
    google.maps.event.addListener(infowindow, 'domready', function() {
        // Reference to the DIV that wraps the bottom of infowindow
        var iwOuter = $('.gm-style-iw');

        /* Since this div is in a position prior to .gm-div style-iw.
        * We use jQuery and create a iwBackground variable,
        * and took advantage of the existing reference .gm-style-iw for the previous div with .prev().
        */
        var iwBackground = iwOuter.prev();

        // Removes background shadow DIV
        iwBackground.children(':nth-child(2)').css({'display' : 'none'});

        // Removes white background DIV
        iwBackground.children(':nth-child(4)').css({'display' : 'none'});

        // Hace más chica el área transparente p/que puedan arrastrar
        setTimeout(function(){iwBackground.parent().css('width','300px');},500);

        // Moves the shadow of the arrow 76px to the left margin. - no sirve
        // iwBackground.children(':nth-child(1)').attr('style', function(i,s){ return s + 'left: 145px !important;'});

        // Moves the arrow 76px to the left margin. - no sirve
        // iwBackground.children(':nth-child(3)').attr('style', function(i,s){ return s + 'left: 145px !important;'});

        // Changes the desired tail shadow color.
        iwBackground.children(':nth-child(3)').find('div').children().css({'box-shadow': 'rgba(72, 181, 233, 0.6) 0px 1px 6px', 'z-index' : '1', 'background-color': '#333'});

        // Reference to the div that groups the close button elements.
        var iwCloseBtn = iwOuter.next();

        // Apply the desired effect to the close button
        iwCloseBtn.css({opacity: '1', right: '-10px', top: '3px', 'border-radius': '13px', 'box-shadow': '0 0 5px #3990B9'});
        iwCloseBtn.next().css({right: '-15px'}); // Cuando se abre en móvil se agrega una imagen invisible donde se debe dar click, se mueve a la derecha por el movimiento que se hizo en el botón

        // If the content of infowindow  exceed the set maximum height, then the gradient is added.
        if($('.iw-content').height() >= 400){
            iwOuter.parent().append('<div class="iw-bottom-gradient"></div>');
        }

        // The API automatically applies 0.7 opacity to the button after the mouseout event. This function reverses this event to the desired value.
        iwCloseBtn.mouseout(function(){
            $(this).css({opacity: '1'});
        });
    });


    google.maps.event.addListener(marker, 'spider_click', function() {
        if(window.mobilecheck()) $('#modal-info-audio').modal('show').find('.modal-body').html(infowindow.content);
        else infowindow.open(marker.get('map'), marker);
        // marker.get('map').setCenter(infowindow.getPosition());
    });
}

function addStaticMarker() {
    $.ajax({
        method: 'GET',
	    url: '/api/audio/',
        headers: {
            'x-csrftoken': $().get_csrf()
        },
        success: function(data){
            contador(data.length);
            addStaticMarkerAjax(data);
        },
        error: function(jqXHR, textStatus, errorThrown){
            mostrarError('Hubo un error al cargar la información');
        }
    });
}


$('#upload-button').on('click', function(){
    var fields = {
        title: document.querySelector('#upload-title').value,
        audio: document.querySelector('#audio'),
        record_place: document.querySelector('#upload-record-place').value
    };
    
    if (validateUploadForm(fields) !== true) {
        mostrarError('Faltan campos');
    }
    else if(validateFile(document.getElementById('audio')) !== true) {
        // La función de validación muestra el mensaje de error
    }
    else {
        send_data();
    }
});

change_pass.addEventListener('click', function(){
    var fields = {
        actual: document.getElementById('actual_password').value ? document.getElementById('actual_password') !== null : '',
        pass1: document.getElementById('password-1').value ? document.getElementById('password-1') !== null : '',
        pass2: document.getElementById('password-2').value ? document.getElementById('password-2') !== null : ''
    };
    if(validateUploadForm(fields) === true && document.querySelector('#password-1').value === document.querySelector('#password-2').value){
        change_password();
    }
    else{
        mostrarError('Las contraseñas no coinciden');
    }
});

makePin.addEventListener('click', function () {
    var session = document.getElementById("session").value;
    if (session == 'true'){
        var card = new Card();
        var data;
        card.removeAllByClass('card');
        removeMarker();
        addMarker(lat, lng);
        uploadForm.setAttribute('style', 'right: 0;');
        popup.setAttribute('style', 'visibility: collapse;');
        latlng = lat + ',' + lng;
        data = getGeoData('create_card');
    }
    else{
        var modal = document.querySelector('.popup-click');
        modal.setAttribute('style', 'display: none;');
        $('#modal-login').modal('show');

    }
});


cierra.addEventListener('click', function(){
    removeMarker();
});
/* Funcionalidades de la app */

var _createClass = function () {
    function defineProperties(target, props) {
        for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
        }
    }
    return function (Constructor, protoProps, staticProps) {
        if (protoProps) defineProperties(Constructor.prototype, protoProps);
        if (staticProps) defineProperties(Constructor, staticProps); return Constructor;
    };
}();

function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
        throw new TypeError("Cannot call a class as a function");
    }
}

var Card = function () {
  function Card() {
    _classCallCheck(this, Card);
  }

  _createClass(Card, [{
    key: 'create',
    value: function create(id, text) {
      var div = document.createElement('div');
      var span = document.createElement('span');
      var a = document.createElement('a');
      div.id = 'card-' + id;
      div.textContent = text;
      div.className = 'card';
      a.dataset.modal = '#modal-upload-form';
      a.textContent = 'Usar ubicación';
      a.className = 'open-modal';
      a.dataset.geo = id;
      span.appendChild(a);
      div.appendChild(span);
      return div;
    }
  }, {
    key: 'removeAllByClass',
    value: function removeAllByClass(className) {
      var cards = document.querySelectorAll('.' + className);
      if (cards.length > 0) {
        cards.forEach(function (element, index, array) {
          element.parentNode.removeChild(element);
          return false;
        });
      }
      return false;
    }
  }, {
    key: 'defaultCard',
    value: function defaultCard(className) {
      var div = document.createElement('div');
      var span = document.createElement('span');
      var a = document.createElement('p');
      div.textContent = 'Puedes cambiar el nombre de la ubicación después de dar click en "Usar ubicación", si te equivocaste puedes crear una nueva';
      div.className = 'card  close-modal';
      div.dataset.modal = '#modal-upload-form';
      a.textContent = 'Eliminar ubicación actual';
      a.dataset.modal = '#modal-upload-form';
      a.className = 'delete-marker open-modal';
      a.addEventListener( 'click', function(){
          removeMarker();
          uploadForm.setAttribute('style', 'display: none;');
        } );
      span.appendChild(a);
      div.appendChild(span);

      return div;
    }
  }]);

  return Card;
}();

// funcion para crear tarjetas
function createCard() {
    var card = new Card();
    var newDefaultCard;
    for (var place in geo_data) {
        var text = geo_data[place].formatted_address;
        var newCard = card.create(place, text);
        uploadForm.appendChild(newCard);
        setCardListener(newCard);
    }

    newDefaultCard = card.defaultCard();
    uploadForm.appendChild(newDefaultCard);
    setCardListener(newDefaultCard);
}

// listeners para tarjetas creadas
function setCardListener(newCard) {
    var element = newCard.querySelector('.open-modal');
    if (element !== null) {
        element.addEventListener('click', function () {
            setModalData(element.dataset.geo);
            $(element.dataset.modal).modal('show');
        });
    }

}

// asignar geo data al modal para subir audios
function setModalData(geoItem) {
    var inputForm = document.querySelector('.js-place-form');
    var inputLocation = document.querySelector('#location_data');
    var setLat = document.querySelector('#upload_lat');
    var setLng = document.querySelector('#upload_lng');
    var geometry = document.querySelector('#upload_geometry');

    inputForm.value = geo_data[geoItem].formatted_address;
    inputLocation.value = JSON.stringify(geo_data[geoItem].address_components);
    setLat.value = lat;
    setLng.value = lng;
    geometry.value = JSON.stringify(geo_data[geoItem].geometry);
}


/* FUNCIONES PARA EL MODAL DE SUBIR AUDIOS */
var agreeButton = document.querySelector('#agree');
var uploadButton = document.querySelector('#upload-button');

agree.addEventListener('change', function () {
  if (this.checked === true)
    uploadButton.disabled = false;
  else
    uploadButton.disabled = true;
});

function validateUploadForm(fields) {
    for (var item in fields) {
        if (fields[item] === null || fields[item] === '') {
            return false;
        }
    }
    return true;
}

function validateFile(element){
    var fileExp = /^(?:[\w]\:\\*)(\\*[a-zA-Z_\-\s0-9\.]+)+\.(mp3|ogg|m4a)$/;
    if(element.value.match(fileExp)) {
        return true;
    } else {
        mostrarError("Revisa que el nombre del archivo no tenga acentos para que tu audio pueda subirse correctamente.");
        element.focus();
        return false;
    }
}

buscar.addEventListener('click', function(e){
    get_audio(to_search);
    $('#modal-buscador').modal('hide');
});

button_tags.addEventListener('click', function(){
    get_tags();
});

// lista de audios
function get_audio(buscar){
    csrf = $().get_csrf();
    buscar = (to_search == 'tag') ? 1 : (to_search == 'estado') ? 2 : (to_search == 'fonografista') ? 3 : (to_search == 'título') ? 5 : 4;
    $.ajax({
        method: 'POST',
	    url: '/api/get_audio/',
        headers: {
            'x-csrftoken': csrf
        },
        data: {
            busqueda: $('#busqueda').val(),
            tipo: buscar
        },
        success: function(data) {
            removeStaticMarker();
            if (data.status == 404){
                mostrarErrorBusqueda();
            }
            else if (!$.trim(data)){
                mostrarErrorBusqueda();
            }
            else {
                map.setCenter({lat:23.9698885, lng: -99.1179815});
                map.setZoom(5);
                contador(data.length);
                addStaticMarkerAjax(data);
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
            console.error('Error: ' + textStatus + " " + errorThrown);
        }
    });
}

function cosas(e){
    $(".navbar-collapse").collapse('hide');
    var target = (e.target.id).replace('tag-', '');
    get_tag_by_state(target);
}

// lista de audios por estado
function estados_tag(e){
    $(".navbar-collapse").collapse('hide');
    $('#busqueda').val((e.target.id).replace('tag-', ''));
    to_search = 'tag';
    get_audio('tag');
}

// lista de tags por fonografista
function get_tags_by_fono(e){
    $(".navbar-collapse").collapse('hide');
    $('#busqueda').val((e.target.id).replace('tag-', ''));
    to_search = 'wakawaka';
    get_audio('wakawaka');
}

// lista de audio por autor
function get_audio_by_author(e){
    $('#busqueda').val((e.target.id).replace('tag-', ''));
    to_search = 'wakawaka';
    get_audio('wakawaka');
}

// lista de cantidad de tags por estado
function get_tags(){
    csrf = $().get_csrf();
    var cantidad_tags = 0;
    $.ajax({
        method: 'GET',
	    url: '/api/get_tag/',
        headers: {
            'x-csrftoken': csrf
        },
        success: function(data){
            estados = data.estados;
            $('#ul_tags').html('');
            $('#ul_tags').append('<li onclick="get_all_tags()"><a data-toggle="modal" data-target="#modal-tags">Todos los Tags</a></li><li><a href="#" onclick="show_all_audios()">Mostrar Todo</<></li>');
            for(var i in estados){
                cantidad_tags += estados[i].cantidad;
                $('#ul_tags').append('<li><a href="#" id="tag-'+ estados[i].estado +'" onclick="cosas(event)">' + estados[i].estado +' ('+ estados[i].cantidad +')</a><ul id="tag_'+i+'" class="ul_inside"></ul></li>');
                for (var j in estados[i].tags){
                    $('#tag_'+i).append('<li class="li_inside"><a href="#" id="tag-'+ estados[i].tags[j] +'" onclick="estados_tag(event)">'+ estados[i].tags[j] +'</a></li>');
                }
            }
        },
        error: function(){
            mostrarErrorBusqueda();
        }
    });
}

// lista de tags por estado
function get_tag_by_state(state){
    $.ajax({
        method: 'POST',
	    url: '/api/get_tag/',
        headers: {
            'x-csrftoken': csrf
        },
        data: {
            estado: state
        },
        success: function(data){
            removeStaticMarker();
            if (data.status == 404) {
                mostrarErrorBusqueda();
            }
            else {
                contador(data.length);
                addStaticMarkerAjax(data)
            }
        },
        error: function() {
            mostrarErrorBusqueda();
        }
    });
}

// lista de todos los tags
function get_all_tags(page_number){
    $(".navbar-collapse").collapse('hide');
    data_tags = '';
    total_num_pags = '';
    $.ajax({
        method: 'POST',
        url: '/api/get_all_tags/',
        headers: {
            'x-csrftoken': $().get_csrf()
        },
        data: {
            page: page_number ? page_number : 1
        },
        success: function(data){
            data_tags = data.tags[0];
            total_num_pags = data.num_pages;
            $('#content').html('');
            for (var i in data_tags){
                $('#content').append('<p id="tag-'+ data_tags[i].tag_name +'" onclick="estados_tag(event)">'+ data_tags[i].tag_name +'</p>');
            }
            $('#page-selection').bootpag({
                total: total_num_pags,
                maxVisible: 20
            }).on("page", function(event, num){
                get_all_tags(num);
            });
        },
        error: function() {
            mostrarErrorBusqueda();
        }
    });
}

// lista de los fonografistas con mas tags
function get_fonografista(){
    $.ajax({
        method: 'GET',
        url: '/api/get_fonografista/',
        success: function(data){
            $('#ul_fono').html('');
            $('#ul_fono').html('<li onclick="get_all_fonos()"><a data-toggle="modal" data-target="#modal-fonografistas">Todos Los Fonografistas</a></li><li><a href="#" onclick="show_all_audios()">Mostrar Todo</a></li>');
            for (var i in data){
                $('#ul_fono').append('<li class="li_inside"><a href="#" id="tag-'+ data[i].username +'" onclick="get_tags_by_fono(event)" >'+ data[i].username +'</a></li>');
            }
        },
        error: function(){
            mostrarErrorBusqueda();
        }
    });
}

// lista de todos los fonogratistas
function get_all_fonos(page_number){
    $(".navbar-collapse").collapse('hide');
    data_fonografista = '';
    total_num_pags = '';
    $.ajax({
        method: 'POST',
        url: '/api/get_all_fonos/',
        headers: {
            'x-csrftoken': $().get_csrf()
        },
        data: {
            page: page_number ? page_number : 1
        },
        success: function(data){
            data_tags = data.tags[0];
            total_num_pags = data.num_pages;
            $('#content-2').html('');
            for (var i in data_tags){
                $('#content-2').append('<p id="tag-'+ data_tags[i].username +'" onclick="get_audio_by_author(event)">'+ data_tags[i].username +'</p>');
            }
            $('#page-selection-2').bootpag({
                total: total_num_pags,
                maxVisible: 20
            }).on("page", function(event, num){
                get_all_fonos(num);
            });
        },
        error: function(){
            mostrarErrorBusqueda();
        }
    });
}

document.getElementById('btn-dropdown').onclick=function(){
// Remove any element-specific value, falling back to stylesheets
};

$(function(){
    $(".input-group-btn .dropdown-menu li a").click(function(){
        var selText = $(this).html();
        to_search = $(this)[0].innerText.toLowerCase();
        $(this).parents('.input-group-btn').find('.btn-search').html(selText);
    });

    $('#navigation-menu-dropdown').find('li').first().click();
    
    // Tooltip de inputs
    $('.tooltipped').tooltip({
        placement: "bottom",
        trigger: "focus"
    });

    // Muestra instrucciones la primera vez que cargan la página
    var firstTime = localStorage.getItem("first_time");
    if(!firstTime) {
        // first time loaded!
        $('#modal-instructions').modal('show');
        localStorage.setItem("first_time","1");
    }
});

// funcion para obtener el csrt token
(function( $ ){
    $.fn.get_csrf = function() {
        var end, start;
        start = document.cookie.indexOf('csrftoken=');
        end = document.cookie.indexOf(";", start);
        if (-1 === end) {
            end = document.cookie.length;
        }
        csrftoken = document.cookie.substring(start, end).replace('csrftoken=', '');
        return csrftoken;
    };
})( $ );

function login(){
    try{
        if (valor !== '' || valor !== null){
            $('#error_message').text(document.querySelector('#error').value);
            $('#modal-login').modal('show');
        }
        else if(valor == 'change'){
            $('#error_message').text(data.deatil);
            $('#modal-cambiar').modal('show');
        }
    }
    catch(err) {
        valor = '';
    }

}

function contador(counter){
    counter_element = document.getElementById('audio_counter');
    counter_element.innerText = counter;
}

function send_data(){
    var form = $('#upload-form')[0];
    var ajaxData = new FormData(form);
    ajaxData.append('photo', $('#upload-photo'));
    ajaxData.append('audio_file', $('#audio'));
    show_loading();
    $('#upload-button').addClass("disabled");
    $.ajax({
        type: 'POST',
        url: '/upload-audio/',
        headers: {
            'x-csrftoken': $().get_csrf()
        },
        enctype: 'multipart/form-data',
        data: ajaxData,
        processData: false,
        contentType: false,
        success: function(){
            hide_loading();
            $('#upload-button').removeClass("disabled");
            mostrarExito('Tu audio ha sido recibido. Gracias por formar parte de la comunidad del Mapa Sonoro de México. Te avisaremos vía correo electrónico cuando tu audio sea publicado.');
            $('#modal-upload-form').modal('hide');
            $(':input','#upload-form')
              .val('')
              .removeAttr('checked')
              .removeAttr('selected');
            $('#info_aside').css("display", "none");
            removeMarker();
        },
        error: function(data){
            hide_loading();
            $('#upload-button').removeClass("disabled");
            mostrarError($.parseJSON(data.responseText).message.replace("['", "").replace("']", ""));
        }
    });
}

function change_password(){
    $.ajax({
        type: 'POST',
        url: '/',
        headers: {
            'x-csrftoken': $().get_csrf()
        },
        data: {
            'actual_password': $('#actual_password').val(),
            'password-1': $('#password-1').val(),
            'action': $('#action').val()
        },
        success: function(data){
            mostrarExito(data.detail);
            $('#modal-cambiar').modal('hide');
            location.reload();
        },
        error: function(data){
            $('#error_message-2').text(data.responseJSON.detail);
        }
    });
    valor = 'change';
}

show_all.addEventListener('click', function(){
    show_all_audios();
});

function show_all_audios(){
    $(".navbar-collapse").collapse('hide');
    to_search = 'all';
    $('#busqueda').val('');
    get_audio(to_search);
}

function search(e){
    if (e.keyCode == 13) {
        document.getElementById('buscar_tipo').click();
        return false;
    }
}

/* Loading */
function show_loading() {
    $('#spinner').removeClass("hidden");
}
function hide_loading() {
    $('#spinner').addClass("hidden");
}

/* GPS */
function current_location() {
    // Try HTML5 geolocation.
    if (navigator.geolocation) {
        show_loading();
        navigator.geolocation.getCurrentPosition(function(position) {
            hide_loading();
            var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            map.setZoom(15);
            map.setCenter(pos);

            setTimeout(function() {
                var overlay = new google.maps.OverlayView();
                overlay.draw = function() {};
                overlay.setMap(map);
                showCreatePin({
                    latLng: pos,
                    pixel: overlay.getProjection().fromLatLngToContainerPixel(pos)
                });
            },500); // Está con timeout p/que el mapa esté centrado cuando se ejecute
        }, function() {
            hide_loading();
            mostrarError("Debes autorizar a la aplicación para que consulte tu ubicación");
        });
    } else {
        mostrarError("Tu navegador no soporta la geolocalización");
    }
}
