var modals = document.querySelectorAll('.open-modal');
var closes = document.querySelectorAll('.modal-close');
var closePopup = document.querySelectorAll('.popup-close');

if (modals.length > 0) {
  modals.forEach(function (element, index, array) {
    element.addEventListener('click', function () {
      var modalId = this.dataset.modal;
      openModal(modalId);
    })
  })
}

if (closes.length > 0) {
  closes.forEach(function (element, index, array) {
    element.addEventListener('click', function () {
      var modalId = this.parentNode.parentNode.id;
      closeModal(modalId);
    })
  })
}

if (closePopup.length > 0) {
  closePopup.forEach(function (element, index, array) {
    element.addEventListener('click', function () {
      this.parentNode.setAttribute('style', 'visibility: collapse;')
    })
  })
}

function openModal(modalId) {
  var modal = document.querySelector(modalId);
  modal.classList.add('in');
}

function closeModal(modalId) {
  var modal = document.querySelector('#' + modalId);
  var modalContent = modal.querySelector('.modal-content');
  modal.classList.remove('modal-active');
  modal.classList.remove('modal-content-active');
}
