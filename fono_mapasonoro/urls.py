"""fono_mapasonoro URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import include, path, re_path
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin

from web import views
from userprofile.views import UserProfile
from audio import urls as audiourls
from django.contrib.auth import views as auth


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', UserProfile.as_view(), name='home'),
    url(r'^api/', include(audiourls)),
    url(r'^upload-audio/', views.upload_audio, name='subir-audio'),
    url(r'^logout/', auth.logout, {'next_page': '/'}, name='logout'),
    # url(r'^signin/$', views.sign_in, name='sign-in'),
    # url(r'^signup/$', views.sign_up, name='sign-up'),
]

admin.site.site_title = 'Mapa Sonoro'
admin.site.site_header = 'Mapa Sonoro'
admin.site.index_title = 'Administración'