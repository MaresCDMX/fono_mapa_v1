# -*- coding: utf8 -*-
from django.core.mail import EmailMessage
from django.conf import settings


def nueva_contra(email, usuario, contra):
    asunto = 'Solicitud de cambio de contraseña'
    mensaje = "Hola {}, tu nueva contraseña es {}".format(usuario, contra)
    mail = EmailMessage(asunto, mensaje, from_email=settings.EMAIL_HOST_USER, to=[email])
    mail.send()