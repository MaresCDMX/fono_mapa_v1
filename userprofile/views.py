import sys
from django.shortcuts import render, redirect
from django.db.models import Q
from django.shortcuts import HttpResponse, HttpResponseRedirect
import json
from django.contrib.auth.models import User
from audio.models import Audio, Tag
from django.contrib.auth.models import User
from django.views.generic import View
from django.contrib.auth import authenticate, login
from django.core.exceptions import ObjectDoesNotExist
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.utils.encoding import smart_str
from django.contrib import messages
import uuid
from .mails import nueva_contra
from web.models import About, Instructions
from django.core.paginator import Paginator
import sys
# sys.stdout.buffer.write(chr(9986).encode('utf8'))



class UserProfile(View):
    def get(self, request):
        message = None
        instrucciones = ''
        tags = Tag.objects.all()
        try:
            about = About.objects.latest('id')
            instrucciones = Instructions.objects.latest('id')
        except About.DoesNotExist:
            about = ''
        except Instructions.DoesNotExist:
            instrucciones = ''
        except Exception:
            about = ''
            instrucciones = ''
        ctx = {
            'message': message,
            'audio_counter': Audio.objects.count(),
            'about': about,
            'instructions': instrucciones,
            'num_pages': Paginator(tags, 20).num_pages
        }
        return render(request, 'base.html', ctx)

    def post(self, request):
        message = None
        username = request.POST.get('username')
        password = request.POST.get('password')
        email = request.POST.get('email')
        action = request.POST.get('action')
        if action == 'sing_in':
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('/')
            else:
                message = 'Usuario o contraeña incorrecta'
                ctx = {
                    'audio_counter': Audio.objects.count(),
                }
                messages.error(request, message)
                return render(request, 'base.html', ctx)
        elif action == 'sing_up':
            try:
                User.objects.get(Q(email=email) | Q(username=username))
                message = 'usuario ya existente'
                messages.error(request, message)
            except ObjectDoesNotExist:
                user = User.objects.create_user(
                    username=username, email=email, password=password
                )
                login(request, user)
            except Exception as e:
                messages.error(request, e)
            return render(request, 'base.html')
        elif action == 'recuperar':
            try:
                usuario = User.objects.get(email=request.POST.get('email'))
                contra = str(uuid.uuid4())[:20]
                usuario.set_password(contra)
                usuario.save()
                nueva_contra(usuario.email, usuario.username, contra)
                messages.success(
                    request, 'el mensaje ha sido mandado correctamente')
            except User.DoesNotExist as e:
                messages.error(request, e)
            except Exception as e:
                messages.error(request, e)
            return HttpResponseRedirect('.')
        elif action == 'cambiar':
            try:
                usuario = User.objects.get(id=request.user.id)
                if usuario.check_password(request.POST.get('actual_password')):  # NOQA
                    usuario.set_password(request.POST.get('password-1'))
                    usuario.save()
                    status = 200
                    detail = 'password updated'
                else:
                    detail = 'Contraseña actual erronea'
                    status = 404
            except ObjectDoesNotExist:
                detail = 'Contraseña actual erronea'
                status = 404
            except Exception as e:
                detail = e.message
                status = 400
            return HttpResponse(
                json.dumps({'status': status, 'detail': detail}),
                content_type='application/json',
                status=status
            )

